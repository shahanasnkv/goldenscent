<?php
/**
 * GoldenScent Observer
 *
 * @category    GoldenScent
 * @package     GoldenScent_PartnerOrders
 * @author      Shahanas Kv <shahanasnkv@gmail.com>
 */

class GoldenScent_PartnerOrders_Model_Observer
{
    /**
     * Set partner data to order (controller_action_predispatch)
     *
     * @return GoldenScent_PartnerOrders_Model_Observer
     */
    public function setPartnerCookie(){
        $partnerName = Mage::app()->getRequest()->getParam('partner');

        if($partnerName){
            $period = 24 * 60 * 60; // 24 hours
            Mage::getSingleton('core/cookie')->set(GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER, $partnerName, $period, '/');
        }

        return $this;
    }

    /**
     * Set partner data to order (sales_order_place_before)
     *
     * @param Varien_Object $observer
     * @return GoldenScent_PartnerOrders_Model_Observer
     */
    public function savePartnerData($observer)
    {
        /* @var $order Mage_Sales_Model_Order */
        $order = $observer->getEvent()->getOrder();

        $partnerName = Mage::getSingleton('core/cookie')->get(GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER);
        $partnerName = $partnerName?$partnerName:'';

        $order->setData(GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER, $partnerName);

        return $this;
    }

    /**
     * Auto create split invoice (sales_order_place_after)
     *
     * @param Varien_Object $observer
     * @return GoldenScent_PartnerOrders_Model_Observer
     */
    public function autoSplitInvoice($observer)
    {
        /* @var $moduleHelper GoldenScent_PartnerOrders_Helper_Data */
        $moduleHelper = Mage::helper('partnerorders');

        /* @var $order Mage_Sales_Model_Order */
        $order = $observer->getEvent()->getOrder();

        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoices = Mage::getModel('sales/order_invoice')->getCollection()
            ->addAttributeToFilter('order_id', array('eq'=>$order->getId()));
        $invoices->getSelect()->limit(1);

        if ((int)$invoices->count() !== 0) {
            return $this;
        }

        if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW && $moduleHelper->isPartnerOrder($order) != false) {

            try {
                if(!$order->canInvoice()) {
                    $order->addStatusHistoryComment('Auto Invoice : Order cannot be invoiced.', false);
                    $order->save();
                }


                $splitItems = $moduleHelper->getSplitItems($order);

                $orderShippingAmount        = $order->getShippingAmount();
                $baseOrderShippingAmount    = $order->getBaseShippingAmount();
                $shippingInclTax            = $order->getShippingInclTax();
                $baseShippingInclTax        = $order->getBaseShippingInclTax();

                foreach($splitItems as $splitQty) {

                    if (count($splitItems) > 1) {
                        $order->setShippingAmount($orderShippingAmount / 2);
                        $order->setBaseShippingAmount($baseOrderShippingAmount / 2);

                        $order->setShippingInclTax($shippingInclTax / 2);
                        $order->setBaseShippingInclTax($baseShippingInclTax / 2);
                    }

                    $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($splitQty);

                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                    $invoice->register();

                    $invoice->getOrder()->setCustomerNoteNotify(false);
                    $invoice->getOrder()->setIsInProcess(true);
                    $order->addStatusHistoryComment('Automatically invoiced(split) by system.', false);

                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($invoice)
                        ->addObject($invoice->getOrder());

                    $transactionSave->save();
                }

                $order->setShippingAmount($orderShippingAmount);
                $order->setBaseShippingAmount($baseOrderShippingAmount);

                $order->setShippingInclTax($shippingInclTax);
                $order->setBaseShippingInclTax($baseShippingInclTax);

                foreach($splitItems as $splitQty) {

                    $shipment = $order->prepareShipment($splitQty);
                    $shipment->register();

                    $order->setIsInProcess(true);
                    $order->addStatusHistoryComment('Automatically shipped(split) by system.', false);

                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($shipment)
                        ->addObject($shipment->getOrder())
                        ->save();
                }
            } catch (Exception $e) {
                $order->addStatusHistoryComment('Auto Invoice : Exception message: '.$e->getMessage(), false);
                $order->save();
            }
        }

        return $this;
    }
}