<?php
/**
 * Order invoice shipping total calculation model
 *
 * @category    GoldenScent
 * @package     GoldenScent_PartnerOrders
 * @author      Shahanas Kv <shahanasnkv@gmail.com>
 */

class GoldenScent_PartnerOrders_Model_Order_Invoice_Total_Shipping extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        /* @var $moduleHelper GoldenScent_PartnerOrders_Helper_Data */
        $moduleHelper = Mage::helper('partnerorders');

        $invoice->setShippingAmount(0);
        $invoice->setBaseShippingAmount(0);
        $orderShippingAmount        = $invoice->getOrder()->getShippingAmount();
        $baseOrderShippingAmount    = $invoice->getOrder()->getBaseShippingAmount();
        $shippingInclTax            = $invoice->getOrder()->getShippingInclTax();
        $baseShippingInclTax        = $invoice->getOrder()->getBaseShippingInclTax();
        if ($orderShippingAmount) {
            if(!$moduleHelper->isPartnerOrder($invoice->getOrder())){
                /**
                 * Check shipping amount in previous invoices
                 */
                foreach ($invoice->getOrder()->getInvoiceCollection() as $previusInvoice) {
                    if ($previusInvoice->getShippingAmount() && !$previusInvoice->isCanceled()) {
                        return $this;
                    }
                }
            }

            $invoice->setShippingAmount($orderShippingAmount);
            $invoice->setBaseShippingAmount($baseOrderShippingAmount);
            $invoice->setShippingInclTax($shippingInclTax);
            $invoice->setBaseShippingInclTax($baseShippingInclTax);

            $invoice->setGrandTotal($invoice->getGrandTotal()+$orderShippingAmount);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal()+$baseOrderShippingAmount);
        }
        return $this;
    }
}
