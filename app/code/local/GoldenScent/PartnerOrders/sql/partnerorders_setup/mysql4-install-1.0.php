<?php
/**
 * GoldenScent
 *
 * @category    GoldenScent
 * @package     GoldenScent_PartnerOrders
 * @author      Shahanas Kv <shahanasnkv@gmail.com>
 */

/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute("quote", GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER, array("type"=>"varchar"));
$installer->addAttribute("order", GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER, array("type"=>"varchar","grid"=>true));

$installer->endSetup();