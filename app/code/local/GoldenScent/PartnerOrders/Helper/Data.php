<?php
/**
 * GoldenScent Helper
 *
 * @category    GoldenScent
 * @package     GoldenScent_PartnerOrders
 * @author      Shahanas Kv <shahanasnkv@gmail.com>
 */

class GoldenScent_PartnerOrders_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PARTNER_ORDER = 'partner_order';

    /**
     * Check partner order
     *
     * @param Mage_Sales_Model_Order $order
     * @return Boolean
     */

    public function isPartnerOrder($order){
        return $order->getData(self::PARTNER_ORDER)?$order->getData(self::PARTNER_ORDER):false;
    }

    /**
     * Split order items
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function getSplitItems($order){
        $splitItems = array();
        $index = 0;
        foreach($order->getAllVisibleItems() as $item){
            $splitItems[$index][$item->getId()] = $item->getQtyOrdered();
            $index = $index?0:1;
        }
        return $splitItems;
    }
}