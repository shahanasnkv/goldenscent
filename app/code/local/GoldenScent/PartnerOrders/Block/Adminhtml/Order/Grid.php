<?php
/**
 * Order Grid Block
 *
 * @category    GoldenScent
 * @package     GoldenScent_PartnerOrders
 * @author      Shahanas Kv <shahanasnkv@gmail.com>
 */

class GoldenScent_PartnerOrders_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareColumns()
    {
        $this->addColumnAfter(GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER, array(
            'header'    => Mage::helper('catalog')->__('Partner Name'),
            'index'     => GoldenScent_PartnerOrders_Helper_Data::PARTNER_ORDER,
            'type' => 'text'
        ),'grand_total');

        return parent::_prepareColumns();
    }
}